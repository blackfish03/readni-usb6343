Example Title:           ReadDigPort

Example Filename:        ReadDigPort.sln

Category:                DI

Description:             This project is modified from the ReadDigPort example provided by NI. It demonstrates how to read a single value
                         from a digital port continuously.

Software Group:          Measurement Studio

Required Software:       Visual Studio .NET (compatiable with VS 2013 and 2015)

Language:                Visual C#

Language Version:        8.0

Driver Name:             DAQmx

Driver Version:          17.1
